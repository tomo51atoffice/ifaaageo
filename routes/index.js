var express = require('express');
var router = express.Router();
var fs = require ('fs');
//var jd = require ('jsdom');                                                                                                
var gamedata = require('./gamedata');


/* GET home page. */

router.post('/', function (req, res) {
    var name="";
    var latitude="";
    var longitude="";

    if (req.body.name) {
        name = req.body.name;
        name = "hoge";
        latitude = req.body.latitude;
        longitude = req.body.longitude;

        console.log (name);
        console.log (latitude);
        console.log (longitude);

        // add your Game Management here                                                                                     
        // your function                                                                                                     
        gamedata.adduser(name);
        var result = gamedata.updateuser(name, latitude, longitude);
        console.log(result);
        var resData = result;

        res.writeHead (200, {"Content-Type": "text/plain"});
        res.write(resData);
    } else {
        res.send ("Error: Not found parameters");
    }
    res.end ();
});

router.get('/game', function (req, res) {
    res.render('game', { title: 'IFAAA Game'});
});

router.get('/result', function (req, res) {
    // userInfo has name and point (userInfo.name, userInfo.point)                                                           
    var userInfo = [{"name":"gora", "point": 50}, {"name": "matsumoto", "point": 70}];

    // add your get point in here                                                                                            
    var name = req.query.name;
    var result = gamedata.getresult();
    for (var i = 0, len = result.length; i < len; i++) {
        userInfo.push(result[i]);
    }
    console.log(userInfo);

    res.render ('result', { title: 'Live Point', "userInfo": userInfo});
});

router.get('/find', function (req, res) {
    fs.readFile ('./geo.html', function (err, html) {
        var name = req.body.name;

//      doc = jsdom.jsdom(html);                                                                                             
//      console.log (doc);         
//      var newNode = document.createElement('div');                                                                         
//      newNode.id = 'name';                                                                                                 
//      newNode.style = 'display/non';                                                                                       
//      newNode.appendChild (name);                                                                                          
//      html.appendChild (newNode);                                                                                          
	console.log ("find process");
	res.writeHeader(200, {"Content-Type": "text/html"});
        res.write(html);
        res.end();
    });
});

router.post('/adduser', function (req, res) {
    var name = req.body.name;

    console.log("add user passed");

    // Add your user name here for Game Management                                                                           
    gamedata.adduser(name);

    // redirect to another page                                                                                              
    res.location ("find");
    res.redirect ("find");
});

module.exports = router;

