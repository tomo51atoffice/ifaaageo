/*jshint devel: true */
/*global IFAAA: true */
/*global require: true */

'use strict';

/*                                                                                                                           
    IFAAAexecute(location, params...);                                                                                       
                                                                                                                             
                                                                                                                             
    script := AT location THEN action;                                                                                       
    location := '(' lat ',' lng ')'                                                                                          
    lat := float                                                                                                             
    lng := float                                                                                                             
    action := GET integer                                                                                                    
                                                                                                                             
    ex.                                                                                                                      
    at (123.45, 43.233) then get 100;                                                                                        
                                                                                                                             
    internal format                                                                                                          
    {'at': '(xxx.xx, yyy.yy)', 'then': ''}                                                                                   
                                                                                                                             
 */
module.exports = (function() {
    var fs = require('fs');
    var scripts = [];

    // example                                                                                                               
    /*                                                                                                                       
    scripts[0] = {                                                                                                           
        'at': {                                                                                                              
            'lat': 123.5,                                                                                                    
            'lng': 43.5                                                                                                      
            },                                                                                                                   
        'then': {                                                                                                            
            'command': 'get',                                                                                                
            'target': 'point',                                                                                               
            'param': 100                                                                                                     
            }                                                                                                                    
	    };                                                                                                                       
    scripts[1] = {                                                                                                           
        'at': {                                                                                                              
            'lat': 124.5,                                                                                                    
            'lng': 43.5                                                                                                      
            },                                                                                                                   
        'then': {                                                                                                            
            'command': 'get',                                                                                                
            'target': 'point',                                                                                               
            'param': 50                                                                                                      
            }                                                                                                                    
	    };                                                                                                                       
    scripts[2] = {                                                                                                           
        'at': {                                                                                                              
            'lat': 123.5,                                                                                                    
            'lng': 44.5                                                                                                      
            },                                                                                                                   
        'then': {                                                                                                            
            'command': 'get',                                                                                                
            'target': 'point',                                                                                               
            'param': -100                                                                                                    
            }                                                                                                                    
	    };                                                                                                                       
    */

    function _parse() {
        var sources = fs.readFileSync('script.ifa') + '';
	var tokens = [];

	/*                                                                                                                   
        fs.readFile('script.ifa', 'utf8', function(err, text) {                                                              
            source = text;                                                                                                   
            console.log(text);                                                                                               
            console.log('error:');                                                                                           
            console.log(err);                                                                                                
            });                                                                                                                  
        */
        // console.log(sources);                                                                                             

        var source = sources.replace(/\s*/, ' ');
        var script_source = source.split(';');
        //console.log(script_source);                                                                                        

        for (var i = 0; i < script_source.length; i++) {
            if (script_source[i].length > 1) {
                tokens = script_source[i].match(/[\w.-]+/g);
                /*                                                                                                           
                console.log('script: ' + i);                                                                                 
                console.log(' length: ' + tokens.length);                                                                    
                for (var j in tokens) {                                                                                      
                    console.log(j + ': ' + tokens[j]);                                                                       
                    }                                                                                                            
                */

                scripts[i] = {
                    'at': {
                        'lat': tokens[1] - 0,
                        'lng': tokens[2] - 0
                    },
                    'then': {
                        'command': tokens[4],
                        'target': tokens[5],
                        'param': tokens[6] - 0
                    }
                };

                //                scripts[i] = script;                                                                       
            }
        }
    }

    function _getTriggers() {
        var triggers = [];

        for (var i = 0; i < scripts.length; i++) {
            triggers[i] = scripts[i].at;
        }

        return triggers;
    }


    function round(num) {
        return Math.round(num * 10) / 10;
    }

    function find_script(lat, lng) {
        var la = round(lat);
	var ln = round(lng);

        for (var i = 0; i < scripts.length; i++) {
	    if (round(scripts[i].at.lat) === la && round(scripts[i].at.lng) === ln) {
		return i;
	    }
        }

        return -1;
    }

    function _execute(lat, lng, data) {
        var index = find_script(lat, lng);
        console.log('found script:' + index);
	if (index < 0) {
            return 'not found';
        }

	if (scripts[index].then.command === 'get') {
            data[scripts[index].then.target] += scripts[index].then.param;

            return 'Get ' + scripts[index].then.param;
        }

        return 'cannot get';
    }


    function _dump_scripts() {
        for (var i = 0; i < scripts.length; i++) {
            console.log(scripts[i]);
            console.log(scripts[i].at.lat);
        }
    }

    return {
        parse: _parse,
        dump_scripts: _dump_scripts,
        execute: _execute,
        getTriggers: _getTriggers
    };
})();

/*    **** the followings are sample ****                                                                                    
                                                                                                                             
// test                                                                                                                      
var users = [];                                                                                                              
var result;                                                                                                                  
// var err, source;                                                                                                          
                                                                                                                             
users['yutani'] = {                                                                                                          
    'point': 0                                                                                                               
    };                                                                                                                           
users['hishinuma'] = {                                                                                                       
    'point': 0                                                                                                               
    };                                                                                                                           
                                                                                                                             
                                                                                                                             
IFAAA.parse();                                                                                                               
                                                                                                                             
// var triggers = IFAAA.getTriggers();                                                                                       
// console.log(triggers);                                                                                                    
                          console.log(users);                                                                                                          
//IFAAA.dump_scripts();                                                                                                      
result = IFAAA.execute(43.5, 123.4, users['yutani']);                                                                        
console.log(result);                                                                                                         
console.log(users);                                                                                                          
                                                                                                                             
result = IFAAA.execute(43.22, 123.49, users['hishinuma']);                                                                   
console.log(result);                                                                                                         
console.log(users);                                                                                                          
                                                                                                                             
result = IFAAA.execute(42.1, 125.21, users['hishinuma']);                                                                    
console.log(result);                                                                                                         
console.log(users);                                                                                                          
*/
